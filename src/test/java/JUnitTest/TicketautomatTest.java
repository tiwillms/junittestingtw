package JUnitTest;


import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static junit.framework.TestCase.*;
import static org.mockito.Mockito.when;

/**
 * Die Testklasse für den Ticketautomaten. Dieses Projekt wurde nach dem TDD prinzip entworfen
 * Zuerst die Tests schreiben und dann die Methoden ausimplementieren. Danach Refactor und schauen ob noch alles passt.
 * Zusätzlich wurde noch für einen Testfall Mockito verwendet
 */
public class TicketautomatTest {

    Ticketautomat ta;

    Ticketautomat tamock = Mockito.mock(Ticketautomat.class);

    Ticket ticket;

    /**
     * Setup der Testklasse. Diese Methode wird vor jedem Test ausgeführt
     * Es wird ein neuer Ticketautomat erstellt und ein neues Ticket (wird für Mockito benötigt)
     */
    @Before
    public void start(){
        //erstellen eines Ticketautomat mit Kosten pro Ticket
        ta = new Ticketautomat(20);
        ticket = new Ticket("Normales Ticket", 0.7);
    }

    /**
     * Testet ob das Geldeinschmeißen Funktioniert
     * Testet auch ob Fehler möglich wären (z.b. negatives Geld)
     */
    @Test
    public void GeldeinschmeisenTest(){
        ta.geldeinschmeisen(20);
        assertEquals(20.0, ta.getEingeschmissenesGeld());

        //Wird ein falscher wert eingeworfen soll trotzdem noch 20 Sein
        ta.geldeinschmeisen(-20);
        assertEquals(20.0, ta.getEingeschmissenesGeld());
    }

    /**
     * Testet ob das Geld nach dem einschmeißen richtig gespeichert wurde
     */
    @Test
    public void gibeingeschmissenesGeldTest(){
        assertEquals(0.0, ta.getEingeschmissenesGeld());
        ta.geldeinschmeisen(20);
        assertEquals(20.0,ta.getEingeschmissenesGeld() );
    }

    /**
     * Testet das Drucken eines Tickets (Ticket not Null)
     * Testet ob das Ticket bei zu wenig Geld nicht gedruckt wird (Ticket Null)
     */
    @Test
    public void TicketDruckenTest(){
        ta.geldeinschmeisen(25);
        Ticket t = ta.ticketDrucken();
        assertNotNull(t);
        assertEquals( 5.0 , ta.getEingeschmissenesGeld() );

        Ticket t2 = ta.ticketDrucken();
        assertNull(t2);
    }

    /**
     * Testet ob der Richtige Preis für das Ticket mit Rabatt angezeigt wird
     * Dafür wurde in der Start ein Ticket erstellt
     */
    @Test
    public void gibRabattTest(){
        double ticketRabatt = ta.berechneRabattPreis(ticket);
        assertEquals( 20*0.7 , ticketRabatt);
    }

    /*Mögliche weitere Methode
        Rabattanzeigen
        Ticket mit Rabatt drucken (mit Ticketdrucken mit rabatt (abziehen des Rabattpreises))
        GeldAuswerfenfunktion ( bzw. Wechselgeldfunktion, Geld retour)
     */

    /**
     * Mockito Test für das Ausdrucken eines Tickets mit Rabatt
     * Zurückgegeben wird ein Ticket wenn genug gGeld eingeschmissen wurde
     *
     */
    @Test
    public void gibTicketmitRabattTest(){
        ta.geldeinschmeisen(20);

        //Die Logik für TicketmitRabatt
        when(tamock.ticketmitRabatt()).thenReturn(ticket);

        System.out.println(ta.getEingeschmissenesGeld());
        Ticket testticket = null;

        if(ta.getEingeschmissenesGeld() >= 16) {

            testticket = tamock.ticketmitRabatt();
            ta.setBetrag(ta.getEingeschmissenesGeld() - ta.berechneRabattPreis(ticket));

        }
        System.out.println(ta.getEingeschmissenesGeld());
        assertEquals(6.0, ta.getEingeschmissenesGeld());
        assertNotNull(testticket);
    }

    /**
     * Testen der geldAuswerfen Methode
     * Geld wird eingeschmissen und danach sollte der Betrag im Automat 0.0 sein
     */
    @Test
    public void geldAuswerfenTest(){
        ta.geldeinschmeisen(20.0);
        //tamock.geldeinschmeisen(20);
        ta.geldAuswerfen();
        assertEquals(0.0, ta.getEingeschmissenesGeld());
    }


}
