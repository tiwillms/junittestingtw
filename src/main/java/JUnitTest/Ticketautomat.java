package JUnitTest;

/**
 * Klasse Ticketautomat
 * Ermöglicht das Erstellen von Tickets
 */
public class Ticketautomat {

    private double ticketpreis;
    private double betrag;

    public Ticketautomat(int preis) {
        this.ticketpreis = preis;
        this.betrag = 0.0;
    }

    /**
     * Gibt den Betrag im Automaten(Eingeschmissenes Geld) zurück
     * @return betrag
     */
    public double getEingeschmissenesGeld() {
        return this.betrag;
    }

    /**
     * Gibt den Preis für ein Ticket zurück
     * @return ticketpreis
     */
    public double getTicketpreis() {
        return this.ticketpreis;
    }

    /**
     * Setzt den Preis für ein Ticket
     * @param ticketpreis
     */
    public void setTicketpreis(double ticketpreis) {
        this.ticketpreis = ticketpreis;
    }

    /**
     * Gibt den Betrag im Ticketautomat zurück
     * @return betrag
     */
    public double getBetrag() {
        return this.betrag;
    }

    /**
     * Setzt den Betrag
     * @param betrag
     */
    public void setBetrag(double betrag) {
        this.betrag = betrag;
    }

    /**
     * Berechnet den Preis (inkl. Rabatt) für ein mitgegebenes Ticket
     * @param t
     * @return rabattpreis
     */
    public double berechneRabattPreis(Ticket t) {
        return ticketpreis * t.getRabatt();
    }

    /**
     * Ermöglicht das einschmeißen von Geld und Speichert den Betrag falls dieser gültig ist
     * @param geld
     */
    public void geldeinschmeisen(double geld) {
        if(geld > 0.0) {
            this.betrag = geld;
        }
    }

    /**
     * Druckt ein Ticket und gibt es wenn genug Geld vorhanden ist zurück
     * @return Ticket
     */
    public Ticket ticketDrucken() {
        Ticket newticket;
        if(this.betrag >= 20) {
            newticket = new Ticket("Hauptticket", 0.7);
            setBetrag(this.betrag - ticketpreis);
        }else{
            System.out.println("Zu wenig Geld eingeworfen!");
            return null;
        }
        System.out.println("Ticket wird gedruckt");
        return newticket;
    }

    /**
     * Gibt das Restgeld zurück
     */
    public void geldAuswerfen() {
        System.out.println("Sie erhalten " + getEingeschmissenesGeld() + " zurück");
        this.betrag = 0;
    }

    /**
     * Methode für Mockito (Logik wird von Mockito bzw im Testfall definiert)
     */
    public Ticket ticketmitRabatt() {
        /*if(this.betrag >= 16) {
            Ticket t = new Ticket("Ticket mit Rabatt", 0.3);
            double neuerpreis = (double)ticketpreis - ;
            return neuerpreis;
        }else{
            throw new IllegalArgumentException("Nicht genug Geld");
        }*/
        return null;
    }
}
