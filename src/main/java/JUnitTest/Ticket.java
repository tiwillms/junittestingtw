package JUnitTest;

/**
 * Klasse Ticket. Ein Ticket besitzt eine Bezeichnung und einen Rabattwert
 */
public class Ticket {

    private String bezeichnung;
    private double rabatt;

    public Ticket(String bezeichnung, double rabatt) {
        this.bezeichnung = bezeichnung;
        this.rabatt = rabatt;
    }

    /**
     * Gibt die Bezeichnung des Tickets zurück
     * @return bezeichnung
     */
    public String getBezeichnung() {
        return bezeichnung;
    }

    /**
     * setzt die Bezeichnung eines Tickets
     * @param bezeichnung
     */
    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    /**
     * Gibt den Rabatt zurück
     * @return rabatt
     */
    public double getRabatt() {
        return this.rabatt;
    }

    /**
     * setzt den Rabatt neu
     * @param rabatt
     */
    public void setRabatt(double rabatt) {
        this.rabatt = rabatt;
    }
}
